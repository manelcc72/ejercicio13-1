package es.manelcc.t13ej1;

import android.opengl.GLSurfaceView;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by manelcc on 31/10/2015.
 */
public class MyRenderer implements GLSurfaceView.Renderer {

    private float red = 0.9f;
    private float blue = 0.2f;
    private float green = 0.2f;

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {

    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        // Definimos la superficie en relación al tamaño de la pantalla
        gl.glViewport(0, 0, width, height);
    }

    @Override
    public void onDrawFrame(GL10 gl) {

        gl.glClearColor(red, green, blue, 1.0f);
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
    }


    public void setColor(float r, float g, float b) {
        // Cambiamos los colores
        red = r;
        green = g;
        blue = b;
    }
}
