package es.manelcc.t13ej1;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.view.MotionEvent;

/**
 * Created by manelcc on 31/10/2015.
 */
public class MyView extends GLSurfaceView {
    private final MyRenderer myRenderer;

    public MyView(Context context) {
        super(context);

        myRenderer = new MyRenderer();
        setRenderer(myRenderer);

    }

    @Override
    public boolean onTouchEvent(final MotionEvent event) {
        //return super.onTouchEvent(event);
        myRenderer.setColor(event.getX() / getWidth(),
                event.getY() / getHeight(), 1.0f);
        return true;
    }
}
